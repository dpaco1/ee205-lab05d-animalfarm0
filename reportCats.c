/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 21_Feb_2022
////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "reportCats.h"

int printCat(int index){
   // Check if index is less than 0 or more than number of cats in database
   if (index > MAX_CATS || index < 0){
      printf("animalFarm0: Bad cat [%d]\n", index);
      }
   else {
      printf("cat index = [%u] | name = [%s] | gender = [%d] | breed = [%d] | isFixed = [%d] | weight = [%f]\n", index, nameData[index], genderData[index], breedData[index], boolData[index], weightData[index]);
      }
   return 0;
}

int printAllCats(void){
   // Print all cats by stepping up from i
   for (int i = 0; i < CURRENT_CATS; ++i){
      printf("cat index = [%u] | name = [%s] | gender = [%d] | breed = [%d] | isFixed = [%d] | weight = [%f]\n", i, nameData[i], genderData[i], breedData[i], boolData[i], weightData[i]);  
   }
   return 0;
}

int findCat(char name[]){
   // Scan for name in database
   for (int i = 0; i < MAX_CATS; ++i){
      if (name == nameData[i]){
         printf("Cat %s is in index %u\n", nameData[i], i);
      }
   // Returns index number if found
      return i;
   }
   // Returns error if not found
   return 0;
}
