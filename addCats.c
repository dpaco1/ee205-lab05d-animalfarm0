/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 17_Feb_2022
////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "addCats.h"
#include "catDatabase.h"

int addCat( char name[], int gender, int breed, bool isFixed, float weight){

   // Check if database is full
   if (CURRENT_CATS > MAX_CATS){
         printf("%d is more than the maximum of %d cats.\n", CURRENT_CATS, MAX_CATS);
         return 0;
         }
   
   // Check if cat name is blank
   if (strlen(name) <= 0 ){
      printf("Give the cat a name... not a blank.\n");
      return 0;
      }

   // Check if cat name is longer than 30 characters
   if (strlen(name) > MAX_NAME_LENGTH){
      printf("The cat's name length of %ld is longer than %d.\n", strlen(name), MAX_NAME_LENGTH);
      return 0;
      }
   
   // Check if cat name is unique throughout all of database
   for (int i = 0; i <= MAX_CATS; ++i){
      if (name == nameData[i]){
         printf("There already is a %s in our database.\n", name);
         return 0;
      }
   }
   
   // Check if cat weight is greater than 0
   if (weight <= 0){
      printf("The cat must weigh greater than 0.\n");
      return 0;
      }
  
   // If validations all pass, send data into database arrays and add a number of cats.
   CURRENT_CATS++;
   strncpy(nameData[CURRENT_CATS], name, MAX_NAME_LENGTH);
   genderData[CURRENT_CATS] = gender;
   breedData[CURRENT_CATS] = breed;
   boolData[CURRENT_CATS] = isFixed;
   weightData[CURRENT_CATS] = weight;
   
   return 0;

}
