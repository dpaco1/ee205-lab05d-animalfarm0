/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 21_Feb_2022
////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "updateCats.h"

int updateCatName(int index, char newName[]){
   // Check that index is not less than 0 or more than max number of cats
   if (index < 0 || index > MAX_CATS){
      printf("animalFarm0: Bad Cat [%d]\n", index);
      return 0;
      }
   // Check that newName is not blank
   if (strlen(newName) <= 0){
      printf("New cat name cannot be blank.\n");
      return 0;
      }
   // Check that newName is not longer than max length
   if (strlen(newName) > MAX_NAME_LENGTH){
      printf("The new cat name is longer than %d.\n", MAX_NAME_LENGTH);
      return 0;   
      }
   // Check that newName is unique
   for (int i = 0; i < MAX_CATS; ++i){
      if (newName == nameData[i]){
         printf("There is already a %s in our database.\n", newName);
         return 0;
         }
   }
   // If validations all pass, update cat name
   strncpy(nameData[index], newName, MAX_NAME_LENGTH);
   return 0;
}

int fixCat(int index){
  // Check that index is not less than 0 or greater than max number of cats
  if (index < 0 || index > MAX_CATS){
     printf("animalFarm0: Bad cat [%d]\n", index);
     return 0;
     }
  // Fix the cat
  boolData[index] = true;
  return 0;
}

int updateCatWeight(int index, float newWeight){
   if (index < 0 || index > MAX_CATS){
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 0;
      }
   if (newWeight < 0){
      printf("New weight must be greater than 0.\n");
      return 0;
      }
   weightData[index] = newWeight;
   return 0;
}
