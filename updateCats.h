/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 21_Feb_2022
////////////////////////////////////
#pragma once

int updateCatName(int index, char newName[]);

int fixCat(int index);

int updateCatWeight(int index, float newWeight);
