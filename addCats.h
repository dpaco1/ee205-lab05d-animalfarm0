/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 21_Feb_2022
////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#pragma once

extern int addCat( char name[], int gender, int breed, bool isFixed, float weight);

// extern int addCat( name[], gender, breed, isFixed, weight);

