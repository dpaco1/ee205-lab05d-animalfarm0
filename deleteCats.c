/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 21_Feb_2022
////////////////////////////////////
#include <stdio.h>

#include "catDatabase.h"
#include "deleteCats.h"

int deleteAllCats(void){
   CURRENT_CATS = 0;
   return 0;
}
